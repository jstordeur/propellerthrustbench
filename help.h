/**
* @desc this the description of the HELP command
* Everything in included in the `help` namespace
* The callback function is the one that are used by the parser
* @author Stordeur Jordan stordeur.jordan@gmail.com
* https://gitlab.com/jstordeur/propellerthrustbench
*/

#ifndef __HELP__
#define __HELP__

#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

namespace help{
  const char command[] = "help";

  void callback(CmdParser *myParser) {
     Serial.println("=========");
     Serial.println("- help : get this help");
     Serial.println("- setpoint <option> : utse to set point of motor");
     Serial.println("=========");
  }
}

#endif

#ifndef __SETPOINT__
#define __SETPOINT__

#include <SServo.h>
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

#include "hardware.h"
#include "def.h"

#define MAX_SETPOINTS 50
#define STEP_TIME 1000000

namespace setpoint{
  // Structure to store a point
  struct status {
    unsigned long int timestamp;
    int rpm;
    float torque;
    float strain;
    int temperature;
  };

  // Pseudo delay that can be brake if Serial read something
  bool delay_breakable(unsigned long time);

  // Function to handle CLI
  void help();
  void error();

  void dumpStatus();
  void dumpLog();
  bool launchCampain();
  bool setCampain(char* arg);
  void dumpCampain();
  void logLine(int rpm, float torque, float strain, int temperature);
  void stop();
  void reset();

  // Functions that interact with the real world
  bool setRPM(int rpm);
  float readAmpere();
  byte readTemperature();
  float calcTorque(int rpm);

  // Variables
  const char command[] = "setpoint";

  bool canRun = false;
  bool isCampainSet = false;
  int campain_setpoints[MAX_SETPOINTS];
  struct status campain_status[MAX_SETPOINTS];
  size_t campain_size = 0;
  int current_setpoint;

  const float efficiency[13] = {0, .18, .6, .7, .75, .77, .77, .75, .72, .7, .67, .65, .64};

  void callback(CmdParser *myParser) {
    if(myParser->equalCmdParam(1, "status")) {
      dumpStatus();
    }
    else if(myParser->equalCmdParam(1, "stop")) {
      stop();
    }
    else if(myParser->equalCmdParam(1, "reset")) {
      stop();
      reset();
    }
    else if(myParser->equalCmdParam(1, "force")) {
      char* sp = myParser->getCmdParam(2);
      if(sp != NULL) {
        if(!setRPM(atoi(sp)))
          error();
      }
      else {
        error();
      }
    }
    else if(myParser->equalCmdParam(1, "dumpLog")) {
      dumpLog();
    }
    else if(myParser->equalCmdParam(1, "campain")) {
      if(myParser->equalCmdParam(2, "launch")) {
        launchCampain();
      }
      else if(myParser->equalCmdParam(2, "get")) {
        dumpCampain();
      }
      else if(myParser->equalCmdParam(2, "set")) {
        // TODO
        if(myParser->equalCmdParam(3, "calibration")) {
          Serial.println("calibration");
        }
        else if(myParser->equalCmdParam(3, "csv")) {
          Serial.println("Parsing csv file");

          if(!setCampain(myParser->getCmdParam(4)))
          error();
        }
        else {
          error();
        }
      }
      else {
        error();
      }
    }
    else if(myParser->equalCmdParam(1, "help")) {
      help();
    }
    else {
      error();
    }
  }

  void help() {
    Serial.println("=========");
    Serial.println("Support arguments :");
    Serial.println("- status : print out the actual setpoint");
    Serial.println("- dumpLog : print out log into csv format");
    Serial.println("- stop : stop motor");
    Serial.println("- reset : stop then reset stored campain");
    Serial.println("- force : set a setpoint");
    Serial.println("- campain [get|launch|set] : campain relate commands");
    Serial.println(" - get : print out the stored campain");
    Serial.println(" - launch : launch the stored campain");
    Serial.println(" - set [calibration|csv] : set a new campain");
    Serial.println("  - calibration : set calibration curve");
    Serial.println("  - csv : set datas in CVS format. Each data must be in [0,100]");
    Serial.println("- help : print out those lines");
    Serial.println("Example : ");
    Serial.println("- setpoint campain set csv 50,100,0 : store de campain from a CSV data formated. The data are 50 100 and 0");
    Serial.println("=========");
  }

  void error() {
    Serial.println("Arguments unknow. Type <setpoint help> to get more informations");
  }

  void dumpStatus() {
    Serial.println("Actual set point " + String(current_setpoint) + " is " + String(campain_setpoints[current_setpoint]));
  }

  void dumpLog(){
    Serial.println("timestamp,rpm,torque,strain,temperature");
    for(size_t i = 0; i < campain_size; i++) {
      Serial.printf("%lu,%d,%f,%f,%d\n", campain_status[i].timestamp, campain_status[i].rpm, campain_status[i].torque, campain_status[i].strain, campain_status[i].temperature);
    }
  }

  // IMPROVE : run this function on second core
  bool launchCampain() {
    if(isCampainSet) {
      Serial.println("Are you sure you want to start this campain ?");
      dumpCampain();
      Serial.println("(y/n)....");

      //Wait for 5s for an input
      Serial.setTimeout(5000);

      String response = Serial.readStringUntil('\n');

      current_setpoint = -1;

      if(response == "y") {
        while(++current_setpoint < campain_size) {

          // Set setpoint
          if(!setRPM(campain_setpoints[current_setpoint])) {
            Serial.println("Error in setting RPM\nAbord !");
            stop();
            return false;
          }

          if(!delay_breakable(STEP_TIME)) {
            stop();
            Serial.println("Operation abord !");
            return false;
          }

          // Log status
          // FIXME
          byte temp = readTemperature();
          float torque = calcTorque(campain_setpoints[current_setpoint]);

		if (torque == -1) {
			stop();
               Serial.println("Wrong current. Operation abord !");
               return false;
		}

          logLine(campain_setpoints[current_setpoint], torque, 0, temp);
        }
        return true;
      }
      else {
        Serial.println("operation abord !");
        return false;
      }
    }
    else return false;
  }

  bool delay_breakable(unsigned long time) {
    unsigned long past = micros();

    while( micros() - past < time) {
      if(Serial.available() > 0) {
        Serial.flush();
        return false;
      }
    }

    return true;
  }

  bool setCampain(char* arg) {
    if(arg == NULL)
    return false;

    reset();

    isCampainSet = true;

    // Parse arg to sepoints
    String buf(arg);
    size_t size = buf.length();
    size_t prev_span = 0;
    size_t span = 0;
    campain_size = 0;

    do {
      span = buf.indexOf(",", prev_span);

      campain_setpoints[campain_size++] = buf.substring(prev_span, span).toInt();

      prev_span = span + 1;

    } while(campain_size < MAX_SETPOINTS && span < size);

    return true;
  }

  void stop() {
    setRPM(0);
  }

  void reset() {
    // Disable the possibility to launch a campain
    isCampainSet = false;

    //Erase current campain setpoints
    for(int i = 0; i < MAX_SETPOINTS; ++i) campain_setpoints[i] = 0;
  }

  void dumpCampain() {
    if(isCampainSet) {
      Serial.println(String(current_setpoint) + " setpoint in the campain :");

      for(size_t i = 0; i < current_setpoint; i++) {
        Serial.println(campain_setpoints[i]);
      }
    }
    else
    Serial.println("No campain set. Try to set one with <setpoint campain set csv \"data\">");
  }

  void logLine(int rpm, float torque, float strain, int temperature) {
    campain_status[current_setpoint].timestamp = micros();
    campain_status[current_setpoint].rpm = rpm;
    campain_status[current_setpoint].torque = torque;
    campain_status[current_setpoint].strain = strain;
    campain_status[current_setpoint].temperature = temperature;
  }

  bool setRPM(int rpm) {
    if(rpm >= 0 && rpm <= MAX_ALLOWED_RPM) {
      Serial.printf("RPM is now %d\n", rpm);

      byte power = (byte)RPM_TO_POWER(rpm);

      brushless_dc.write(power);

      return true;
    }
    return false;
  }

  /** FIXME
   * PT100 is used
   * Must be calibrated
  **/
  /**
   * PT100 is 100 ohm at 0°C and 175.84 at 200°C
   *
   *
  **/
  byte readTemperature() {
    float read_volt = (float)analogRead(TEMPERATURE_PIN) * ADC_TO_VOLT;

    float pt100_resistance = PT100_RES * read_volt / (ADC_REF - read_volt);

    // "Extrapolate is BAD !" Mr. Walmag
    if(pt100_resistance >= PT100_0 && pt100_resistance <= PT100_200) {
      byte temperature = (byte) PT100_RES_TO_CELCIUS(pt100_resistance);
      return temperature;
    }
    else
      return -1; // Yeah I know the return type is byte but Trust me, I'm an... Engineer =D
  }

  float readAmpere() {
    float val = analogRead(AMP_CONVERTER_PIN);
    return val * ADC_TO_VOLT * VOLT_TO_AMP;
  }

  /** FORMULA
   * reminder: the final equation is T=(I*U*E*60)/(rpm*2*pi)
   * T as torque; I as current
   * E as efficiency from datasheet; U as voltage fixed at (12-resistanceVolt)
  **/
  float calcTorque(int rpm) {
    float amp = readAmpere();
    float eff = 0;

	if(amp > 0 && amp < 13)
		eff = efficiency[int(amp)] ;
	else return -1;

    const float power = BRUSHLESS_DC_VOLT * amp * eff;
    const float rot_speed = rpm * 2 * PI / 60.0;

    return rot_speed != 0 ? power / rot_speed : 0;
  }
}

#endif

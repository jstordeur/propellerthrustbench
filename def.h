/**
* @desc this file keep track of each define used in this project
* If you want to mudulate the source code. This may be the entry points
* @author Stordeur Jordan stordeur.jordan@gmail.com
* https://gitlab.com/jstordeur/propellerthrustbench
*/

#ifndef __DEF__
#define __DEF__

#include <SServo.h>

// Command parser
#define MAX_CMD_NUMBER 2 // Number of command that can be read
#define MAX_CMD_SIZE 250 // Number of caracter that can be handle

// ESP ADC
#define ADC_REF 3.3 // ADC voltage reference
#define ADC_TO_VOLT ADC_REF / 4096.0

// Amp-meter - ACS712
#define VOLT_TO_AMP 0.01515151515 // Calculated with ACS712 datasheet

// Temperature meter
#define PT100_0     100.0 // Values of PT100 at 0°C
#define PT100_200   175.84 // Values of PT100 at 200°C

#define PT100_RES   1000 // Value of aditionnal resistance for PT100 calculation

// Conversion from PT100 resistance to degrees in celcius
#define PT100_RES_TO_CELCIUS(res)(res * 2.637130802 - 263.7130802)
#define RPM_TO_POWER(rpm)(rpm / (float)MAX_RPM * MAX_POWER)

// Brushless motor
#define BRUSHLESS_DC_VOLT 12
#define MIN_POWER 0 // Power of BLDC motor is set in percentage
#define MAX_POWER 100
#define MAX_RPM 12000 // FIXME this value must be calibrated
#define MAX_ALLOWED_RPM 6000  // Protect hardware. Must handle the MAX_RPM though

#define MICROS_MIN 1000 // PWM - min ON time // Duty 5%
#define MICROS_MAX 2000 // PWM - max ON time // Duty 10%

Servo brushless_dc;

#endif

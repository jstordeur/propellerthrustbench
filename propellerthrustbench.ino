/**
* @desc this project give to the user the control of a brushless_dc motor.
* It can handle a campain of setpoint and take a measure at each points
* @author Stordeur Jordan stordeur.jordan@gmail.com
* https://gitlab.com/jstordeur/propellerthrustbench
*/

#include <SServo.h>
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

#include "hardware.h"
#include "def.h"

#include "help.h"
#include "setpoint.h"

CmdCallback<MAX_CMD_NUMBER> cmdCallback;

void setup() {
	Serial.begin(115200);

	/**
	* Add the command that can be used
	* each command is defined in their resepective headers
	*/
	cmdCallback.addCmd(help::command, &(help::callback));
	cmdCallback.addCmd(setpoint::command, &(setpoint::callback));

	// Initialize the brushless_dc object
	brushless_dc.attach(BRUSHLESS_DC_PIN, Servo::CHANNEL_NOT_ATTACHED, MIN_POWER, MAX_POWER, MICROS_MIN, MICROS_MAX);

	// Force it to 0 by security
	brushless_dc.write(0);
}

void loop() {
	CmdBuffer<MAX_CMD_SIZE> myBuffer;
	CmdParser     myParser;

	/**
	* This command creates his internal loop hand is going to watch the serial port.
	* If one command is give it will respond in consequences.
	*/
	cmdCallback.loopCmdProcessing(&myParser, &myBuffer, &Serial);
}

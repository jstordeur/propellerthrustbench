/**
* @desc this file include everything that is related to hardware
* If you need to add or del a components. Take a look here
* @author Stordeur Jordan stordeur.jordan@gmail.com
* https://gitlab.com/jstordeur/propellerthrustbench
*/

#ifndef __HARDWARE__
#define __HARDWARE__

#define BRUSHLESS_DC_PIN 5
#define TEMPERATURE_PIN 35
#define AMP_CONVERTER_PIN 4

#endif

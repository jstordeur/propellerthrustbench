# Informations
This source code is hosted on Gitlab https://gitlab.com/jstordeur/propellerthrustbench

This project was made in the context of **turbo laboratory**. The goal was to crate a test bench for different propellers.

The report is linked is the repository.

# Dependencies

**First you will need to install the Arduino IDE :**
- Arduino IDE : https://www.arduino.cc/en/main/software

**Then you will need some library to be able to compile the code :**
- ESP32 on Arduino IDE : https://github.com/espressif/arduino-esp32
- Servo library for ESP32 : https://github.com/RoboticsBrno/ESP32-Arduino-Servo-Library
> For the Servo library you must change the name of the file Servo.h and Servo.cpp into SServo.h and SServo.cpp.
> Then, you will need to edit the SServo.cpp file by changing the #include "Servo.h" into >#include "SServo.h".
> This will prevent conflict with the existing Arduino library.
> This solution can obviously be enhanced

- https://github.com/pvizeli/CmdParser

# Upload
To upload the code, you will have to :
- Choose the right device under **Tools -> Board** in the Arduino libraries.
- Choose the right portcom under **Tools -> Port**
- Click on the arrow button **Upload**

# Calibration
before the use we must calibrate the brushless_dc. This is simply done with the command :
`setpoint force 100`

You will need to measure the RPM for the max power.

Once you have the RPM, you will have to edit the source code at the **line 38 of file def.h** : `#define MAX_RPM y` by replacing y by the value measured.

The calibration is done, you will have to re-upload the code into the ESP32.

We have defined this value because it mustn't change with time because of the ESC.

# Launch a campaign

Make a campaign of measure is kind of simple. You will have to call the commands :

- `setpoint campaign set csv <points>` : this command define a campaign in csv format. `<points>` must be replaced by all the setpoins in RPM.

- `setpoint campaign launch` : launch the campaign.

- `setpoint dumplog` : dump the measurements into a csv format.

Type `help` to get a list of available commands.
Type `<command> help` to get more information about the relative command.

# Thrust acquisition with strain gauge (MultiDAQ software)

After download the MultiDAQ software (available [HERE](https://micro-measurements.com/download?id=11947)), you can plug in the USB StudentDAQ MM01-120.

Before any measurement campaign, it is important to carry out a calibration of the strain gauge. The calibration is carried out by suspending several known masses from the axis of the motor using the plastic part provided for this purpose. For each mass, the MultiDAQ software makes it possible to read the value of the strain gauge, expressed in millivolt.

A calibration line can therefore be performed then giving the relationship between the thrust and the value of the strain gauge.
    
Once this is done, the user unit can be configured in the MultiDAQ software. to be changed depending on the calibration line)

# Source code
The code is self explanatory so nothing will be explain here.

If you want to modulate the code you will need to edit the def.h and hardware.h files.

If you want to edit the code, check the command files help.h and setpoint.h

Nothing is complicated so RTFM of each library if you don't understand. If they're still some trouble check the next section.

# Contact
If anything goes wrong, don't hesitate to contact the main developer at **stordeur.jordan@gmail.com**
